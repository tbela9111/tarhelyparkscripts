# ValidateDate:
# Validate dates for varios logs
#
# *** Example: Syslog ***
# file = File.open("/var/log/system.log").each do |line|
#   if line_date = ValidateDate.syslog(line) then
#     puts ValidateDate.between(line_date, DateTime.parse("2013-11-30 00:00:00"), DateTime.new())
#   end
# end

require 'date'

class ValidateDate
  # Parse date_str to DateTime object
  def self.date(date_str)
    begin
      date_line = DateTime.parse(date_str)
    rescue
      false
    end
  end
  
  # Check if date_str is between begin and end date
  def self.between(date_str, begin_date, end_date)
    if base_date = self.date(date_str) then
      base_date < end_date && base_date > begin_date ? false : base_date
    else
      false
    end
  end

  # Get time from string as syslog format
  def self.syslog(line)
    begin
      match_data = line.match(/^[A-Z][a-z]{2}.[0-9]{2}.[0-9]{2}:[0-9]{2}:[0-9]{2}/)
      self.date match_data[0]
    rescue
      false
    end
  end
  
  def self.mysql(line)
    begin
      match_data = line.match(/(^[0-9]{6}.[0-9 ]{2}:[0-9]{2}:[0-9]{2})/)
      self.date match_data[0]
    rescue
      false
    end
  end
end



require 'lumberg'
require 'mixlib/cli'

module CpanelAuth
	def auth filename
		if File.exists "../etc/#{filename}"
			File.open(filename, "rb").read
		end	
	end

end
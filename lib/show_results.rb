# ShowResults class
#
# Author: Viktor Kovacs<kovvik@gmail.com>
#
# Version: 0.0.1

class ShowResults
  def self.print_line string
    puts string
  end

  def self.print_multiline string_array
    string_array.each { |string| self.print_line string}
  end

  def self.print_table(string_array, tabstop)
  end
end

require_relative "show_results.rb"

class EximLogAnalyzer
  def initialize(date, log_file_name)
    @date = date
    @log_file_name = log_file_name
    # All messages sent by the local SMTP server
    @all_outgoing_messages = Array.new
    # Email addresses sending messages
    @sender_email_addresses = Hash.new(0)
    # Domains sending messages
    @sender_domains = Hash.new(0)
    
    @log_file = File.read(@log_file_name).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
    set_all_outgoing_messages
    count_senders
    show_summary
  end


  # count uniqe senders of outgoing messages
  def count_senders
    sender_email_addresses = Hash.new(0)
    outgoing_domains = Hash.new(0)
    @all_outgoing_messages.each do |outgoing_line|
      # unique email addresses
      sender_email_addresses[outgoing_line["message_sender"]] += 1
      # unique domains
      domain = outgoing_line["message_sender"].match(/@(.*)/).to_s
      domain[0] = ''
      outgoing_domains[domain] += 1
    end
    @sender_email_addresses = sender_email_addresses
    @outgoing_domains = outgoing_domains
    # Do some sorting
    @sender_email_addresses = @sender_email_addresses.sort_by { |x, y|  y}
    @sender_email_addresses.reverse!
    @outgoing_domains = outgoing_domains.sort_by { |x, y|  y}
    @outgoing_domains.reverse!
  end
  
  # count all outgoing massages
  def set_all_outgoing_messages
    all_outgoing_messages = Array.new
    @log_file.scan(/^.* <= .*/) do |outgoing_line|
      next unless @date.to_s == outgoing_line[0..9].to_s
      tmp_line = {
        "message_id" => outgoing_line.match(/......-......-../).to_s,
        "message_sender" => outgoing_line.match(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i).to_s
      }
      all_outgoing_messages << tmp_line
    end
    @all_outgoing_messages = all_outgoing_messages
  end
  
  def show_summary
    ShowResults.print_line "Date: #{@date}"
    puts "All outgoing messages: " + @all_outgoing_messages.count.to_s
    # list first 10 of the most frequent senders
    n = 1
    puts "List the first 10th of the most frequent senders:"
    puts "No. \temail\t\t\t\t\t\tcount"
    puts "============================================================="
    @sender_email_addresses.each do |email_address, count_number|
      puts n.to_s + ".\t" + email_address + "\t\t\t\t" + count_number.to_s
      break if n > 9
      n += 1
    end
    
    puts "-------------------------------------------------------------"
    puts "count of unique senders: " + @sender_email_addresses.count.to_s
    
    
    # list first 10 of the most frequent sender domain
    n = 1
    
    puts "\nList the first 10th of the most frequent sender domains:"
    puts "No. \tdomain\t\t\t\t\t\tcount"
    puts "============================================================="
    @outgoing_domains.each do |domain, count_number|
      puts n.to_s + ".\t" + domain + "\t\t\t\t" + count_number.to_s
      break if n > 9
      n += 1
    end
    
    puts "-------------------------------------------------------------"
    puts "count of unique sender domains: " + @outgoing_domains.count.to_s
  end

end


class EximSummary
  def show_results
    
  end
end

class EximSummaryCLI < EximSummary
  def show_results
    
  end
end

class EximMessage
  attr_accessor :sender, :recipient
  def initialize sender, recipient
    @sender = sender
    @recipient = recipient
  end
end
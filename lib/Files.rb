# 
# 
#
# Author::    Viktor Kovacs  (mailto:kovacs.viktor@codeplay.hu)
# Copyright:: Copyright (c) 2014 CodePlay Solutions Kft.
# License::   

# 


class Files
  def self.get_file_names_filtered(dir, filter)
    files = getFileNames(dir)
    filtered_files = Array.new
    files.each do |filename|
      basename = File.basename(filename)
      if basename =~ filter
        filtered_files << filename
      end
    end 
    return filtered_files
  end
  
  def self.get_file_names(dir, full=true)
    files = Array.new
    Dir.foreach(dir) do |filename|
      next if filename == '.' or filename == '..'
      full ? filename = dir + "/" + filename : false
      files << filename
    end
    return files
  end
end
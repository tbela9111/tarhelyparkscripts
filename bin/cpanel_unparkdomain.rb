#!/usr/bin/env ruby

require 'lumberg'
require 'pp'
require 'mixlib/cli'
require 'csv'
require_relative '../lib/cpanel/Auth.rb'

class CpanelDeleteDomain
	include Mixlib::CLI

	option :host,
		short: 				'-h HOST',
		long: 				'--host HOST',
		description: 	'The hostname of the Cpanel/WHM server, access hash must be in ../etc/{hostname}'

	option :username,
		short: 				'-u USERNAME',
		long: 				'--user USERNAME',
		description: 	'Cpanel username',
		required: 		true

	option :domain,
		short: 				'-d DOMAIN',
		long: 				'--domain DOMAIN',
		description: 	'Domain to Unpark',
		required: 		true

	def run
		host = @config[:host]

		@server = Lumberg::Whm::Server.new(
		  host: @config[:host],
		  hash: Auth.new("../etc/#{host}").key
		)

		user = @server.account.summary(
			username: @config[:username]
		)

		# Test if user exists, quit if not
		unless user[:success] then
			puts "\e[31mUser not found: #{@config[:username]}!\e[0m"
			exit 1
		end

		parked_domains = Lumberg::Cpanel::Park.new(
			server: @server,
			api_username: @config[:username]
		)

		puts "Removing parked domain: #{@config[:domain]} ..."
		output = parked_domains.remove(
				domain: @config[:domain]
			)

		puts output

	end
end

if $0 == __FILE__
  cli = CpanelDeleteDomain.new
	cli.parse_options
	cli.run
end

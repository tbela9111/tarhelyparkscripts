#!/bin/sh
if [ $# -lt 5 ]; then
  echo "Usage:  create_snapshot_2.sh volume_group_name logical_volume_name snapshot_name snapshot_size mount_dir"
  echo "Ex.: create_snapshot_2.sh vg0 lv_vz lv_snapshot 50G /var/backup/vz"
fi

if [ `df -h | grep "$3" | grep -v "grep" | wc -l` -eq 0 ]
then
  /usr/sbin/lvcreate -L$4 -s -n $3 /dev/$1/$2
  echo "Snapshot created from /dev/$1/$2, with name /dev/$1/$3 (size: $4)"
  mount -t auto /dev/$1/$3 $5
  echo "Snapshopt mounted at $5"
else
  echo "Snapshot exists!"
fi

#!/usr/bin/env ruby

require 'etc'

dir = '/tmp'
users = Hash.new(0)
Dir.foreach(dir) do |item|
  next if item == '.' or item == '..'
  # do work on real items
  uid = File.stat("#{dir}/#{item}").uid
  username = Etc.getpwuid(uid).name
  size = File.size("#{dir}/#{item}")
  users[username] += size
end

users = users.sort_by {|_key, value| value}
users.each {|name, size| puts "#{name}: #{size.to_i} B(#{(size.to_f/(1024*1024)).round(3)}MB)"}
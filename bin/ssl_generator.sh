#!/bin/bash
# CA key
openssl genrsa -out ca.key 4096
# CA crt
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
# client key
openssl genrsa -out client.key 1024
# client csr
openssl req -new -key client.key -out client.csr
# client crt
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt
# client p12
openssl pkcs12 -export -out client.p12 -inkey client.key -in client.crt

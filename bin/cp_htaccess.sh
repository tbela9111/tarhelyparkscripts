#!/bin/sh



OWNER=`ls -l .htaccess | awk '{print $3}'`
mv .htaccess .htaccess_old
cat <<EOT >> .htaccess
RewriteEngine On
RewriteOptions Inherit
ErrorDocument 503 /cgi-sys/suspendedpage.cgi
RewriteRule .* - [R=503,L]
EOT
#cp /home/cpadmin/seged/.htaccess .
chown $OWNER:$OWNER .htaccess

#!/usr/bin/env ruby
# Exim log analyzer
#require 'Date'

require_relative '../lib/validate_date.rb'
require_relative '../lib/exim_log_analyzer.rb'

def validate_args argv
  # Default log file
  log_file_name = "/var/log/exim_mainlog"
  # Usage info
  usage = 'Usage: ./exim_log_analyzer.rb [yyyy-mm-dd] [log_file]'
  # Check the argument numbers
  case argv.length
  when 0
    # If there are no arguments, then set day to today
    date = Date.today()
  when 1
    # If there is only one argument, then this is the date 
    date = Date.parse(argv[0])
  when 2
    # If there are two argumetnts, then the first is the date,
    # the second is the log file
    date = Date.parse(argv[0])
    # Check if the file exists
    if File.exists? argv[1] then
      log_file_name = argv[1]
    # If the file doesn't exist, quit
    else
      puts "Error: File does not exist!"
      puts usage
      exit
    end
  else
    # There were more then two arguments, quit 
    puts "Error: Wrong number of arguments!"
    puts usage
    exit
  end
  return date, log_file_name
end

arguments = validate_args ARGV
exim_log_analyzer = EximLogAnalyzer.new(arguments[0], arguments[1])

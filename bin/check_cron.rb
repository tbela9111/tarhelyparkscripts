#!/usr/bin/env ruby
#
# Finds cron jobs at given time from crontab files at a specified location
# 
# Usage: check_cron.rb [time] [location]
#
# Time format (cron): minute[:hour][:day][:month][:weekday]
#
#
#
# Author: Viktor Kovacs<kovvik@gmail.com>
#
# Copyright CodePlay Solutions, 2014
#
# Version: v0.0.1
#

# Returns an array with the times from the column
# Min and max values sets time range
def get_time_from_column(cron, min, max)
	cron_minutes = Array.new
	case cron
	# Single time, with or without leading zero (n)
	when /^[0-9]+$/
		cron_minutes << cron.to_i
		puts "empty"
	# Multiple times, separated by commas (n,m,o)
	when /^([0-9]+,?)+$/
		cron_minutes = cron.split(',').collect {|n| n.to_i}
	# Everytime (*)
	when /^\*$/
		cron_minutes = (min.to_i..max.to_i).to_a
  # Repeating (*/n)
	when /^\*\/[0-9]+$/
		number = cron.match(/\d+/)[0].to_i
		cron_minutes = (min..max).step(number).to_a
	# Not a cron job column
	else
		cron_minutes = false
	end
	return cron_minutes
end


#file = File.open("../temp/crontab").each.with_index do |line, index|
#	p line.match(/^([0-9,\/\*])*\s*([0-9,\/\*])*\s*([0-9,\/\*])*\s*([0-9,\/\*])*\s*([0-9,\/\*])*\s*(.*)$/)[1..6]
#end

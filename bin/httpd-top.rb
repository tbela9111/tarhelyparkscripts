#!/usr/bin/env ruby

file = '/home/kovvik/Work/temp/httpd_fullstatus'

class HttpdStatusParser
  def initialize(httpd_status_file)
    lines = Array.new()
    File.open(httpd_status_file).each.with_index do |line, index|
      next unless line =~ /^\s{3}[0-9]*-[0-9]*\s(-|[0-9]*)\s/
      lines <<  line + IO.readlines(httpd_status_file)[index + 1] 
    end
    puts lines
  end
  
end

class HttpdStatus
  def initialize 
    
  end
end

httpd_status = HttpdStatusParser.new(file)
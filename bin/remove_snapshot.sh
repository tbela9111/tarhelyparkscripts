#!/bin/sh

# Is the snapshot exists?
if [ `df -h | grep "lv_snapshot" | grep -v "grep" | wc -l` -eq 1 ]
then
  # Is the rsync backup ended?
  if [ `ps aux | grep "rsync" | grep -v "grep" | wc -l` -eq 0 ]
  then
    umount /var/backup/vz/
    /usr/sbin/lvremove /dev/vg0/lv_snapshot -f
  else
    "Rsync is still runing!"
  fi
else
  echo "Snapshot doesn't exists!"
fi



#!/usr/bin/env ruby

require 'find'

def print_versions(name, versions)
  all = 0
  puts "==================================="
  puts "*** Found #{name} Versions:   ***"
  puts "-----------------------------------"
  versions.sort.each { |key, value|  puts "#{key}: #{value}"; all += value}
  puts "-----------------------------------"
  puts "Total: #{all}"
  puts "==================================="
end


def check_joomla_version(home_dir)
  if !File.exists?("#{home_dir}/index.php") || !File.open("#{home_dir}/index.php").find { |line|  line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /Joomla/} then
    version = false
  elsif File.exists?(versionfile = home_dir + "/libraries/joomla/version.php") ||
      File.exists?(versionfile = home_dir + "/libraries/cms/version/version.php") || 
      File.exists?(versionfile = home_dir + "/includes/version.php") then
    file = File.open(versionfile)
    release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /RELEASE/}
    release = release.match(/\'(.*)\'/)[1].to_s
    dev_level = file.find { |line| line =~ /DEV_LEVEL/}
    dev_level = dev_level.match(/\'(.*)\'/)[1].to_s
    version = "#{release}.#{dev_level}"
  else
    version = "unknown"
  end
  return version
end

def check_wordpress_version(home_dir)
  if !File.exists?("#{home_dir}/index.php") || !File.open("#{home_dir}/index.php").find { |line|  line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /WordPress/} then
    version = false
  elsif File.exists?(versionfile = home_dir + "/wp-includes/version.php") then
    file = File.open(versionfile)
    release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /\$wp\_version.\=/}
    release = release.match(/\'(.*)\'/)[1].to_s
    version = "#{release}"
  else
    version = "unknown"
  end
  return version
end

def check_virtuemart_version(version_file)
  file = File.open(version_file)
  release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /RELEASE.=/}
  version = release.match(/\'(.*)\'/)[1].to_s
  return version
end

def check_opencart_version(home_dir)
  if File.exists?(versionfile = home_dir + "/index.php") then
    file = File.open(versionfile)
    release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /VERSION/}
    if !release then
      version = "unknown"
    else
      begin
        release = release.match(/\'([0-9\.]*)\'/)[1].to_s
        version = "#{release}"
      rescue
        version = "unknown"
      end
    end
  else
    version = "unknown"
  end
  return version
end

def check_prestashop_version(version_file)
  begin
    file = File.open(version_file)
  rescue
    return "Error"
  end
  release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /_PS_VERSION_/}
  version = release.match(/\'([0-9\.]*)\'/)[1].to_s
  return version
end



def find_configuration_php(path_to_search)
  versions = Hash.new
  Find.find(path_to_search) do |path|
    path = path.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
    if path =~ /(.)?\/configuration.php$/ then
      # Joomla
      home_dir = File.dirname(path).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      if version = check_joomla_version(home_dir) then
        if !versions["Joomla"] then
          versions["Joomla"] = Hash.new(0)
        end
        versions["Joomla"][version] += 1
        puts "Found Joomla: #{home_dir}, version: #{version}"
      end
    # Virtuemart plugin
    elsif path =~ /(.)?\/com_virtuemart\/version\.php/ then
      path = path.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      if version = check_virtuemart_version(path) then
        if !versions["Virtuemart"] then
          versions["Virtuemart"] = Hash.new(0)
        end
        versions["Virtuemart"][version] += 1
        puts "Found Virtuemart: #{path}, version: #{version}"
      end
     #OpenCart
     elsif path =~ /(.)?\/system\/library\/cart\.php/ then
      path = path.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      if version = check_opencart_version(path[0..-24]) then
        if !versions["OpenCart"] then
          versions["OpenCart"] = Hash.new(0)
        end
        versions["OpenCart"][version] += 1
        puts "Found OpenCart: #{path}, version: #{version}"
      end
    #Prestashop
    elsif path =~ /(.)?\/config\/settings\.inc\.php/ then
      path = path.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      if version = check_prestashop_version(path) then
        if !versions["Prestashop"] then
          versions["Prestashop"] = Hash.new(0)
        end
        versions["Prestashop"][version] += 1
        puts "Found Prestashop: #{path}, version: #{version}"
      end
    elsif path =~ /(.)?\wp-config.php/ && !(path =~ /(.)?\/wp-admin/) then
      # Wordpress
      home_dir = File.dirname(path).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      if version = check_wordpress_version(home_dir) then
        if !versions["WordPress"] then
          versions["WordPress"] = Hash.new(0)
        end
        versions["WordPress"][version] += 1
        puts "Found WordPress: #{home_dir}, version: #{version}"
      end
    end
  end
  versions.each { |name, version| print_versions(name, version) }
end


find_configuration_php('.')

#!/usr/bin/env bash

BACKUP_DIR="/var/backup/mysql/dumps/"

if [ ! -d "$BACKUP_DIR" ]; then
  echo "Creating backup directory: $BACKUP_DIR"
  mkdir -p $BACKUP_DIR
fi

for database in $(mysql -e "show databases"|awk -F " " '{print $1}'); do
  echo "Backup database: $database"
  mysqldump --routines $database > $BACKUP_DIR$database.sql
  echo "Done."
done
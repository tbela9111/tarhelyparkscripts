#!/usr/bin/env ruby

# @version 2.0.0

require 'find'
class CmsFinder
  @filenames = Hash.new
  def puts(log)
    if @verbose then
      $stdout.puts log
    end
    if @logfile then
      @logfile.puts log
    end
  end

  def initialize(path_to_search, logfile, verbose=true)
    @verbose = verbose
    @logfile = false
    if logfile != "" then
      begin
        @logfile = File.open(logfile, "w")
      rescue
        puts "ERROR: Log file (#{logfile}) could not be created!"

      end
    end
    @versions = Hash.new
    @cms_datas = [
      {
        name: "Joomla",
        regex: "(.)?\/configuration.php$",
        handler: JoomlaVersion
      },
      {
        name: "Virtuemart",
        regex: "(.)?\/com_virtuemart\/version\.php",
        handler: VirtuemartVersion
      },
      {
        name: "Woocommerce",
        regex: "(.)?\/wp-content\/plugins\/woocommerce\/woocommerce\.php",
        handler: WoocommerceVersion
      },
      {
        name: "Opencart",
        regex: "(.)?\/system\/library\/cart\.php",
        handler: OpenCartVersion
      },
      {
        name: "Prestashop",
        regex: "(.)?\/config\/settings\.inc\.php",
        handler: PrestashopVersion
      },
      {
        name: "WordPress",
        regex: "(.)?\wp-config.php",
        handler: WordPressVersion
      },
      {
        name: "OSClass",
        regex: "(.)?\oc-load.php",
        handler: OSClassVersion
      },
      {
        name: "Drupal",
        regex: "(.)?\/includes\/bootstrap\.inc",
        handler: DrupalVersion
      },
      {
        name: "TinyMCE",
        regex: "(.)?(\/js\)?\/tiny(_)?mce\/tiny(_)?mce\(.min)?\.js",
        handler: TinyMCEVersion
      },
      {
        name: "Magento",
        regex: "(.)?\/app\/Mage\.php",
        handler: MagentoVersion
      }

    ]
    @path_to_search = path_to_search
    # Iterate through files and subdirectories
    Find.find(@path_to_search) do |path|
      # filenames must be converted to utf-8
      path = path.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
      next if path.include?("virtfs")
      # Iterate through the filenames array, and check if the file is in the regex
      @cms_datas.each do |cms_data|
        if path =~ /#{cms_data[:regex]}/ then
          # If the filename is a valid cms file,
          # then call the appropriate version checker
          # and add to the versions array
          version = cms_data[:handler].new(path).version
          # If it is a valid site, then add to @versions, and output some info
          if version then
            puts "Found #{cms_data[:name]} here: #{path}, the version is: #{version}"
            if !@versions["#{cms_data[:name]}"] then
              @versions["#{cms_data[:name]}"] = Hash.new(0)
            end
            @versions["#{cms_data[:name]}"]["#{version}"] += 1
          end
        end
      end
    end
  end

  # Prints versions of the found CMSs, defined by name
  def print_versions(name)
    all = 0
    puts "==================================="
    puts "*** Found #{name} Versions:     ***"
    puts "-----------------------------------"
    @versions["#{name}"].sort.each { |key, value|  puts "#{key}: #{value}"; all += value}
    puts "-----------------------------------"
    puts "Total: #{all}"
    puts "==================================="
  end

  # Prints all found CMSs, all versions

  def print_all_versions
    @versions.each {|key, name| print_versions key}
  end

end

class CmsVersion
  attr_reader :version
  def initialize(path)
  end
end

class JoomlaVersion < CmsVersion
  def initialize(path)
    # Set home directory
    home_dir = File.dirname(path).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
    # Check if is it a valid Joomla site
    if !File.exists?("#{home_dir}/index.php") || !File.open("#{home_dir}/index.php").find { |line|  line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /Joomla/} then
      @version = false
    # Search for files containing version info
    elsif File.exists?(versionfile = home_dir + "/libraries/joomla/version.php") ||
        File.exists?(versionfile = home_dir + "/libraries/cms/version/version.php") ||
        File.exists?(versionfile = home_dir + "/includes/version.php") then
      # Set version from file
      begin
        file = File.open(versionfile)
        release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /RELEASE/}
        release = release.match(/\'(.*)\'/)[1].to_s
        dev_level = file.find { |line| line =~ /DEV_LEVEL/}
        dev_level = dev_level.match(/\'(.*)\'/)[1].to_s
        @version = "#{release}.#{dev_level}"
      rescue
        # If the version info is hidden, the version is unknown
        @version = "unknown"
      end
    else
      # If no files for Joomla version, the version is unknown
      @version = "unknown"
    end
  end
end

class WoocommerceVersion < CmsVersion
  def initialize(path)
    # Set version from file
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /\*\sVersion:/}
      @version = release.match(/\*\sVersion:\s(.*)/)[1].to_s
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class VirtuemartVersion < CmsVersion
  def initialize(path)
    # Set version from file
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /RELEASE.=/}
      @version = release.match(/\'(.*)\'/)[1].to_s
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class OpenCartVersion < CmsVersion
  def initialize(path)
    # Todo: Check if it is a valid OpenCart sites (Hard, because no mention of "Opencart" in files)
    # Set root directory of OpenCart
    home_dir = path[0..-24]
    # Check if it is a site and not a plugin
    if File.exists?(versionfile = home_dir + "/index.php") then
      # Set version from file
      begin
        file = File.open(versionfile)
        release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /VERSION/}
        release = release.match(/\'([0-9\.]*)\'/)[1].to_s
        @version = "#{release}"
      rescue
        # If the version info is hidden, the version is unknown
        @version = "unknown"
      end
    else
      # Not an OpenCart site
      @version = false
    end
  end
end

class WordPressVersion < CmsVersion
  def initialize(path)
    # Set home directory
    home_dir = File.dirname(path).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
    # Check if it is a valid WordPress site
    if !File.exists?("#{home_dir}/index.php") || !File.open("#{home_dir}/index.php").find { |line|  line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /WordPress/} then
      @version = false
    # Search for file containing version info
    elsif File.exists?(versionfile = home_dir + "/wp-includes/version.php") then
      # Set version from file
      begin
        file = File.open(versionfile)
        release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /\$wp\_version.\=/}
        release = release.match(/\'(.*)\'/)[1].to_s
        @version = "#{release}"
      rescue
        # If the version info is hidden, the version is unknown
        @version = "unknown"
      end
    else
      # If no files for WordPress version, the version is unknown
      @version = "unknown"
    end
  end
end

class PrestashopVersion < CmsVersion
  def initialize(path)
    # Todo: Site validation
    # Set version from file
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /_PS_VERSION_/}
      @version = release.match(/\'([0-9\.]*)\'/)[1].to_s
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class DrupalVersion < CmsVersion
  def initialize(path)
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /'VERSION'/}
      @version = release.match(/\'([0-9\.]*)\'/)[1].to_s
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class OSClassVersion < CmsVersion
  def initialize(path)
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /'OSCLASS_VERSION'/}
      @version = release.match(/\'([0-9\.]*)\'/)[1].to_s
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class TinyMCEVersion < CmsVersion
  def initialize(path)
    begin
      file = File.open(path)
      release = file.find { |line| line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil) =~ /majorVersion/}
      version_match = release.match(/majorVersion\:\"([0-9]*)\"\,minorVersion\:"([0-9\.]*)\"/)
      @version = "#{version_match[1].to_s}.#{version_match[2].to_s}"
    rescue
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

class MagentoVersion < CmsVersion
  def initialize(path)
    begin
      file = File.open(path)
      version_hash = Hash.new {}
      file.readlines.each do |line|
        line = line.force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
        if line =~ /\'major\'.*\=\>.*\'.*\'/ then
          version_hash["major"] = line.match(/.*\'(.*)\'/)[1]
        elsif line =~ /\'minor\'.*\=\>/ then
          version_hash["minor"] = line.match(/.*\'(.*)\'/)[1]
        elsif line =~ /\'revision\'.*\=\>/ then
          version_hash["revision"] = line.match(/.*\'(.*)\'/)[1]
        elsif line =~ /\'patch\'.*\=\>/ then
          version_hash["patch"] = line.match(/.*\'(.*)\'/)[1]
        end
      end
      @version = "#{version_hash["major"]}.#{version_hash["minor"]}.#{version_hash["revision"]}.#{version_hash["patch"]}"
    rescue Exception => e
      puts e
      # If the version info is hidden, the version is unknown
      @version = "unknown"
    end
  end
end

find_cms = CmsFinder.new(Dir.pwd, "")
#find_cms.print_versions "Joomla"
find_cms.print_all_versions

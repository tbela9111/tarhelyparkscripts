#!/usr/bin/env ruby

require 'pp'
require 'csv'
#require_relative '../lib/cpanel/Auth.rb'
require_relative '../lib/cpanel/cpanel_auth.rb'

class CpanelDeleteDomain
	include CpanelAuth
	include Mixlib::CLI

	option :host,
		short: 				'-h HOST',
		long: 				'--host HOST',
		description: 	'The hostname of the Cpanel/WHM server, access hash must be in ../etc/{hostname}'

	option :username,
		short: 				'-u USERNAME',
		long: 				'--user USERNAME',
		description: 	'Cpanel username',
		required: 			true

	option :domain,
		short: 				'-d',
		long: 				'--domain',
		description: 	'Domain to remove',
		boolean: 			true,
		required: 		true

	def run
		if @config[:host]
			host_key_file = "../etc/#{@config[:host]}"
		else
			host_key_file = "/root/.accesshash"
		end

		@server = Lumberg::Whm::Server.new(
		  host: @config[:host],
		  hash: auth(host_key_file)
		)

		user = @server.account.summary(
			username: @config[:username]
		)

		unless user[:success] then
			puts "\e[31mUser not found: #{@config[:username]}!\e[0m"
			exit 1
		end


		addondomains = Lumberg::Cpanel::DomainLookup.new(
			server: @server,
			api_username: @config[:username]
		)


		if config[:delete] then
			delete_user
		else
			user_domains = []
			addondomains.list[:params][:data].each do |domain|
				dns_obj = Resolv::DNS.new
				res = dns_obj.getresources domain[:domain], Resolv::DNS::Resource::IN::NS
				res.map! { |r| r.name.to_s }
				user_domains << {
					domain: domain[:domain],
					nameservers: res}
				print_domain domain[:domain], res
				set_ttl domain[:domain] if @config[:ttl]
			end
		end
	end
end

if $0 == __FILE__
  cli = CpanelDeleteDomain.new
	cli.parse_options
	cli.run
end

#!/usr/bin/env ruby

require 'lumberg'
require 'pp'
require 'mixlib/cli'
require 'csv'
require_relative '../lib/cpanel/Auth.rb'

class CpanelDomains
	include Mixlib::CLI

	option :host,
		short: 				'-h HOST',
		long: 				'--host HOST',
		description: 	'The hostname of the Cpanel/WHM server, access hash must be in ../etc/{hostname}',
		required: 		true

	option :username,
		short: 				'-u USERNAME',
		long: 				'--user USERNAME',
		description: 	'Cpanel username',
		required: 			true

	option :ttl,
		short:  			'-t TTL',
		long: 				'--ttl TTL',
		description:  'If set, all the user domains\' TTL is set (main and www subdomain)'

	option :delete,
		short: 				'-d',
		long: 				'--delete',
		description: 	'Deletes user from host',
		boolean: 			true

	def print_domain domain, nameservers
		nameservers.sort!
		puts "#{config[:username]},#{domain},#{nameservers[0]}"
	end

	def get_zone domain
		zone = @server.dns.dump_zone(
			domain: domain
		)
		return zone[:params][:record]
	end

	def set_ttl domain
		
		zone = get_zone domain

		lines = zone.select { |hash| hash[:name] == "#{domain}." && hash[:type] == "A"}
		

		lines.each do |line|
			puts "Old TTL: #{line[:ttl].to_s}"
			output = @server.dns.edit_zone_record(
				domain: 		domain,
				line: 			line[:Line],
				address: 		line[:address],
				ttl: 				@config[:ttl],
				type: 			line[:type],
				class: 			line[:class]
			)

			
			puts "\e[31mSomething went wrong!\e[0m" unless output[:success]
			puts output[:message] 
		end

		new_zone = get_zone domain
		lines = new_zone.select { |hash| hash[:name] == "#{domain}." && hash[:type] == "A"}
		lines.each {|line| puts "New TTL: #{line[:ttl]}"}

		#pp zone.invert[:name => "codeplay.hu."]
	end

	def delete_user
		puts "deleting user: #{config[:username]}"
		output = @server.account.remove(
			username: config[:username],
			keepdns: 1
		)

		puts "\e[31mSomething went wrong!\e[0m" unless output[:success]
		puts output[:message] 
	end

	def run
		host = @config[:host]

		@server = Lumberg::Whm::Server.new(
		  host: @config[:host],
		  hash: Auth.new("../etc/#{host}").key
		)

		user = @server.account.summary(
			username: @config[:username]
		)

		unless user[:success] then
			puts "\e[31mUser not found: #{@config[:username]}!\e[0m"
			exit 1
		end


		addondomains = Lumberg::Cpanel::DomainLookup.new(
			server: @server,
			api_username: @config[:username]
		)

	
		if config[:delete] then
			delete_user
		else
			user_domains = []
			addondomains.list[:params][:data].each do |domain|
				dns_obj = Resolv::DNS.new
				res = dns_obj.getresources domain[:domain], Resolv::DNS::Resource::IN::NS
				res.map! { |r| r.name.to_s }
				user_domains << {
					domain: domain[:domain],
					nameservers: res}
				print_domain domain[:domain], res
				set_ttl domain[:domain] if @config[:ttl] 
			end
		end
	end
end

if $0 == __FILE__
  cli = CpanelDomains.new
	cli.parse_options
	cli.run
end


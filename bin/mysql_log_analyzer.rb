#!/usr/bin/env ruby
# MySQL log analyzer

require_relative '../lib/validate_date.rb'

class MysqlLogAnalyzer
  def initialize arguments
    
    # initialize values
    @log_file_name = arguments[:log_file_name]
    @verbose = arguments[:verbose]
    actual_time = DateTime.new
    @start_time = Time.now
    @connection_ids = Hash.new("")
    @connection_users = Hash.new(0)
    @longest_lines = Hash.new("")
    @user_connections = Hash.new(0)
    line_data = {
      id: 0,
      time: @actual_time, 
      index: 0,
      length: 0,
    }
    
    # If verbose set, calculate the lines in the log file
    if @verbose then
      puts "Calculating lines..."
      @lines = `wc -l #{@log_file_name}`
      @lines = @lines.to_i
      puts "Done"
    end
    # Iterate trhough the log file
    file = File.open(@log_file_name).each.with_index do |line, index|
      # If verbose set print information about the process
      if @verbose then
        percent = (((index + 1).to_f/@lines.to_f)*100.0).to_i
        print "\rExamining lines: #{index + 1}/#{@lines} (#{percent}%)"
      end
      line = line.force_encoding('iso-8859-1').encode('utf-8')
      previous_line_data = line_data
      line_data = parse_line(line, index, actual_time, previous_line_data)
      unless previous_line_data.equal? line_data then
        connection_user = @connection_ids[line_data[:id]].to_s
        @connection_users[connection_user] += line_data[:length]
        @user_connections[connection_user] += 1
        if line_data[:length] > @longest_lines[connection_user].length then
          @longest_lines[connection_user] = line
        end
        # puts "Connection user: #{@connection_ids[previous_line_data[:id]]}, id: #{previous_line_data[:id]}"
      end
    end
    output_results
  end
  
  def parse_line line, index, actual_time, line_data
    
    connection_id = line[0,20].match(/\s([0-9]+)\s/)
    if line[0..33].match(/Connect/) then
      user = line.match(/([0-9]+).Connect.([a-z0-9_]*)/)
      @connection_ids[user[1].to_s] = user[2] if user
    end
    
    if connection_id then
      # puts "connection_id: #{connection_id}"
      if date_time = ValidateDate.mysql(line[0..15]) then
        #puts date_time
      else
        date_time = actual_time
      end
      #p line_data
      new_line_data = {
        id: connection_id[1],
        time: date_time, 
        index: index.to_i,
        length: line.length,
      } 
      return new_line_data
    else
      line_data[:length] += line.length
      return line_data
    end
  end
  
  def output_results
    puts
    
    begin
      file = File.open("results", "w")
      @connection_ids.each { |x, y| file.puts "#{x}: #{y}"}
      @connection_users = @connection_users.sort_by { |x, y|  y}.reverse
      
      file.puts "================================================================="
      file.puts "Sent data by users"
      
      puts "================================================================="
      puts "Sent data by users"
      @connection_users[0..9].each_with_index do |x, y|
        puts "#{y + 1}. #{x[0]}: #{x[1]} B (#{(x[1].to_i)/(1024*1024)} MB)"
        file.puts "#{y + 1}. #{x[0]}: #{x[1]}"
      end
      
      file.puts "================================================================="
      file.puts "Sent lines by users"
      
      puts "================================================================="
      puts "Sent lines by users"
      @user_connections = @user_connections.sort_by { |x, y|  y}.reverse
      @user_connections[0..9].each_with_index do |x, y|
        file.puts "================================================================="
        puts "#{y + 1}. #{x[0]}: #{x[1]}"
        file.puts "#{y + 1}. #{x[0]}: #{x[1]}"
      end
      
      file.puts "================================================================="
      file.puts "Longest lines"
      puts "================================================================="
      puts "Longest lines"
      
      @longest_lines = @longest_lines.sort_by { |x, y|  y.length}.reverse
      @longest_lines[0..9].each_with_index do |x, y|
        file.puts "================================================================="
        puts "#{y + 1}. #{x[0]}(#{x[1].length} B, #{(x[1].length.to_i)/(1024*1024)} MB)"
        file.puts "#{y + 1}. #{x[0]}(#{x[1].length}):\n#{x[1]}"
      end
      time_now = Time.now
      time_elapsed = time_now - @start_time

      
      puts "================================================================="
      puts "Done in #{time_elapsed} seconds (Start time: #{@start_time}, End time: #{time_now}),"
      puts "#{@lines} lines were examined."
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file == nil
    end
  end
end




def validate_args argv
  arguments = Hash.new()
  def print_usage
    puts 'Usage: ./mysql_log_analyzer.rb logfile [ARGUMENTS]'
    puts 'ARGUMENTS: '
    puts '--help: prints this help'
    puts '-v, --verbose: prints information about the analysis during run'
    exit
  end
  if argv.length == 0 or argv.include?("--help") then
    print_usage
  else
    if not File.exists?(argv[0]) then
      puts "File #{argv[0]} doesn't exists."
      print_usage
    else
      arguments[:log_file_name] = argv[0]
      if argv.include?("--verbose") or argv.include?("-v") then arguments[:verbose] = true end
    end
  end
  arguments
end

if $0 == __FILE__
  mysql_log_analyzer = MysqlLogAnalyzer.new validate_args(ARGV)
end




# 1. Sor beolvasás
# 2. Van-e connection id?
#     ha igen akkor az id a connection_id lesz
#     ha nincs akkor a sor az előző sorhoz tartozik - exit
# 3. Időbélyeg a sorból, ha nincs akkor az előző lesz

#!/usr/bin/env ruby
# MySQL log analyzer

if not ARGV.length == 1 then
  puts 'Usage: ./mysql_log_analyzer.rb logfile'
  exit
else
  if not File.exists?(ARGV[0]) then
    puts "File #{ARGV[0]} doesn't exists."
    exit
  else
    log_file = ARGV[0]
  end
end


line_index = {
  "line_number" => 0,
  "line_length" => 0,
  "line" => "",
  "time" => ""
}

line_datetime = "-"
connection_ids = Hash.new

connection_users = Hash.new(0)
file = File.open(log_file).each.with_index do |line, index|
  
  line = line.force_encoding('iso-8859-1').encode('utf-8')
  if datetime = line.match(/(^[0-9]{6}.[0-9 ]{2}:[0-9]{2}:[0-9]{2})/) then
    line_datetime = datetime[1]
  end
  
  
  if line.length > 1000 then
    line_index["line_length"] = line.length
    line_index["line_number"] = index
    line_index["line"] = line
    line_index["time"] = line_datetime
    connection_id = line_index["line"].match(/([0-9]+)/).to_s
    connection_user = connection_ids[connection_id].to_s
    connection_users[connection_user] += 1
    puts "Connection user: " + connection_ids[connection_id].to_s
  end
  if line.match(/Connect/) then
    user = line.match(/([0-9]+).Connect.([a-z0-9]*)/)
    if user then 
      connection_ids[user[1]] = user[2]
    end
  end
end

connection_id = line_index["line"].match(/([0-9]+)/).to_s
puts "Longest line number: " + line_index["line_number"].to_s
puts "Connection id: " + connection_id
puts "Connection user: " + connection_ids[connection_id].to_s
puts "Data ammount: " + line_index["line"].bytesize.to_s + " bytes"
puts "Time: " + line_index["time"].to_s
#puts "Longest line: \n" + line_index["line"]

File.open("results", 'w') do |file|
  file.write("Log file name: " + log_file)
  file.write("Longest line number: " + line_index["line_number"].to_s)
  file.write("Connection id: " + connection_id)
  file.write("Connection user: " + connection_ids[connection_id].to_s)
  file.write "Data ammount: " + line_index["line"].bytesize.to_s + " bytes"
  file.write "Time: " + line_index["time"].to_s
  file.write "Longest line: \n" + line_index["line"]
end

connection_users = connection_users.sort_by { |x, y|  y}
puts connection_users

require_relative "../../bin/check_cron.rb"
require "test/unit"

class TestGetTimeFromColumn < Test::Unit::TestCase
	def test_one_number
		assert_equal([1], get_time_from_column("1", 0, 59))
	end

	def test_one_number_with_leading_zero
		assert_equal([1], get_time_from_column("01", 0, 59))
	end

	def test_multiple_numbers
		assert_equal([1, 2, 3], get_time_from_column("1,2,3", 0, 59))
	end

  def test_asteriks
		assert_equal([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], get_time_from_column("*", 0, 11))
	end

  def test_asteriks_per_number
		assert_equal([0,5,10], get_time_from_column("*/5", 0, 11))
	end

	def test_empty_string
		assert_equal(false, get_time_from_column("", 0, 11))
		assert_equal(false, get_time_from_column("   ", 0, 11))
	end
end

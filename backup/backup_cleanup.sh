#!/bin/bash
MAIN_DIR="/var/backup"
declare -a dirs=( "cpanel2" "cpanel3");
MONDAY_ONE_WEEK=`date -d'monday-7 days' +%Y.%m.%d`
MONDAY_TWO_WEEKS=`date -d'monday-14 days' +%Y.%m.%d`
YESTERDAY=`date -d'yesterday' +%Y.%m.%d`
TODAY=`date -d'today' +%Y.%m.%d`

for DIR in "${dirs[@]}"
do
	WDIR=$MAIN_DIR/$DIR
        cd $WDIR
        DIRS=`ls | grep -v -E "$MONDAY_ONE_WEEK|$MONDAY_TWO_WEEKS|$YESTERDAY|$TODAY|current"`
        echo "Removing the following directories from $WDIR:"
        echo $DIRS
	#rm -rf $()
done

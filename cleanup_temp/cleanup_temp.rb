#!/usr/bin/ruby

require_relative 'inc/Files.rb'
require 'logger'



log = Logger.new('log/cleanup_temp.log', 'daily')
directory = "/home/kovvik/tmp"
timeout = 3600
exclude_files = ["mysql.sock"]

def getFilesToDelete(dir, timeout, excludeFiles)
  files = Array.new()
  directory_files = Files.getFileNames(dir)
  directory_files.each do |filename|
    next if excludeFiles.include? File.basename(filename)
    file = File.new(filename)
    cTime = File.ctime(filename)
    actualTime = Time.new()
    age = (actualTime - Time.at(cTime))
    if age > timeout
      files << filename
    end
  end
  return files
end


def deleteFiles(files, log)
  files.each do |filename|
    if File.directory? filename
      dir = Dir.entries(filename)
      if !(dir - %w{ . .. }).empty?
        deleteFiles(Files.getFileNames(filename), log)
      end
      Dir.rmdir(filename)
      log.info "Directory delete: #{filename}"
    else
      File.delete(filename)
      log.info "File delete: #{filename}"
    end
  end
end
log.info "\r\n================\r\nCleanup started.\r\n================"
filesToDelete = getFilesToDelete(directory, timeout, excludeFiles)
deleteFiles(filesToDelete, log)
log.info "\r\n================\r\nCleanup ended.\r\n================"


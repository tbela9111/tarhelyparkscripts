#!/usr/bin/ruby
require 'etc'
require 'logger'
require_relative 'inc/Files.rb'

log = Logger.new('log/drupal_update_cleaner.log', 'daily') 
dir = "/home/kovvik/tmp"
homeRoot = "/home/"

def getOwnerNames(files)
  owners = Array.new
  files.each do |file|
    name = Etc.getpwuid(File.stat(file).uid).name
    next if owners.include? name
    owners << name
  end
  return owners
end

def setDrupalConfig(user, homeRoot, log)
  userRoot = homeRoot + user
  configFileName = userRoot + "/public_html/sites/default/settings.php" 
  if File.exist?(configFileName)
    if File.read(configFileName).include?("$conf[file_temporary_path]")
      log.warn "Drupal config file already set for user: #{user}"
    else
      File.chmod(0600, configFileName)
      File.open(configFileName, 'a') { |file| file.write("$conf['file_temporary_path'] = '#{userRoot}/tmp';") }
      File.chmod(0400, configFileName)
      log.info "Drupal config file set for user: #{user}"
    end
  else
    log.error "Drupal config file not found for user: #{user}"
  end
end


log.info "\r\n==============================\r\nDrupal Update Cleaner started.\r\n=============================="
files = Files.getFileNamesFiltered(dir, /^update?/)
owners = getOwnerNames(files)
owners.each do |owner|
  setDrupalConfig(owner, homeRoot, log)
end
log.info "\r\n==============================\r\nDrupal Update Cleaner ended.\r\n=============================="



